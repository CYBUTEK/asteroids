﻿using System;
using System.Collections;
using Abstracts;
using Components;
using Managers;
using UnityEngine;

namespace Actors
{
    public class Ship : SingletonBehaviour<Ship>
    {
        [Header("Invincibility")]
        [SerializeField] private float invincibilityDuration = default;
        [SerializeField] private float invincibilityFlashRate = default;

        [Header("Camera Shake")]
        [SerializeField] private float destroyShakeMagnitude = default;

        [Header("Physics")]
        [SerializeField] private float thrust = default;
        [SerializeField] private float torque = default;

        [Header("Weapon")]
        [SerializeField] private GameObject bulletPrefab = default;
        [SerializeField] private Transform bulletSpawn = default;

        [Header("Flames")]
        [SerializeField] private GameObject aftFlame = default;
        [SerializeField] private GameObject portFlame = default;
        [SerializeField] private GameObject starboardFlame = default;

        private float invincibilityElapsedTime;

        public float Alpha
        {
            get => SpriteRenderer ? SpriteRenderer.color.a : 1f;
            set
            {
                if (SpriteRenderer)
                {
                    var color = SpriteRenderer.color;
                    color.a = Mathf.Clamp01(value);
                    SpriteRenderer.color = color;
                }
            }
        }

        public Collider2D Collider2D
        {
            get;
            private set;
        }

        public Rigidbody2D Rigidbody2D
        {
            get;
            private set;
        }

        public SpriteRenderer SpriteRenderer
        {
            get;
            private set;
        }

        public static event Action Destroyed;

        protected override void Awake()
        {
            base.Awake();

            Rigidbody2D = GetComponent<Rigidbody2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            Collider2D = GetComponent<Collider2D>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(out Asteroid _))
            {
                Destroy(gameObject);
            }

            if (collision.gameObject.TryGetComponent(out Enemy enemy))
            {
                Destroy(enemy.gameObject);
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            GameManager.SpawnExplosion(transform.position);
            GameManager.Lives--;
            CameraShake.Magnitude += destroyShakeMagnitude;
            Destroyed?.Invoke();
        }

        private IEnumerator Start()
        {
            if (Collider2D)
            {
                Collider2D.enabled = false;
            }

            while (invincibilityElapsedTime <= invincibilityDuration)
            {
                invincibilityElapsedTime += Time.deltaTime;

                var theta = Mathf.Cos(invincibilityElapsedTime * 360f * invincibilityFlashRate / 2f * Mathf.Deg2Rad);
                Alpha = Mathf.Clamp(Mathf.Abs(theta), 0.2f, 1f);
                yield return null;
            }

            Alpha = 1f;

            if (Collider2D)
            {
                Collider2D.enabled = true;
            }
        }

        private void Update()
        {
            var verticalInput = GameManager.Controls.Primary.Move.ReadValue<Vector2>().y;
            var horizontalInput = GameManager.Controls.Primary.Move.ReadValue<Vector2>().x;
            var fireInput = GameManager.Controls.Primary.Shoot.triggered;

            if (Rigidbody2D)
            {
                if (verticalInput > 0f)
                {
                    Rigidbody2D.velocity += (Vector2)transform.up * thrust * Time.deltaTime;
                }

                if (Mathf.Abs(horizontalInput) > 0f)
                {
                    Rigidbody2D.angularVelocity -= horizontalInput * torque * Time.deltaTime;
                }
            }

            if (fireInput && bulletPrefab && bulletSpawn)
            {
                if (Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation).TryGetComponent(out Bullet bullet))
                {
                    bullet.InteractsWithEnemy = true;
                    bullet.Rigidbody2D.velocity = Rigidbody2D.velocity;
                }
            }

            if (aftFlame)
            {
                aftFlame.SetActive(verticalInput > 0f);
            }

            if (portFlame)
            {
                portFlame.SetActive(horizontalInput > 0f);
            }

            if (starboardFlame)
            {
                starboardFlame.SetActive(horizontalInput < 0f);
            }
        }
    }
}