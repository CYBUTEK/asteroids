﻿using System;
using Abstracts;
using Actors;
using UI.Canvases;
using UI.Menu;
using UnityEngine;
using Utilities;

namespace Managers
{
    public class GameManager : SingletonBehaviour<GameManager>
    {
        private static int lives;
        private static int score;

        [SerializeField] private int asteroidWaveCount = default;
        [SerializeField] private int numberOfLives = default;
        [SerializeField] private float respawnDelay = default;
        [SerializeField] private float minEnemySpawnDelay = default;
        [SerializeField] private float maxEnemySpawnDelay = default;

        [Header("Prefabs")]
        [SerializeField] private GameObject shipPrefab = default;
        [SerializeField] private GameObject asteroidPrefab = default;
        [SerializeField] private GameObject explosionPrefab = default;
        [SerializeField] private GameObject enemyPrefab = default;

        private float timeTillEnemySpawn;

        public static Controls Controls
        {
            get;
            private set;
        }

        public static bool IsPlaying
        {
            get;
            private set;
        }

        public static bool IsQuitting
        {
            get;
            private set;
        }

        public static int Lives
        {
            get => lives;
            set
            {
                lives = Mathf.Clamp(value, 0, int.MaxValue);
                LivesChanged?.Invoke(lives);
            }
        }

        public static int Score
        {
            get => score;
            set
            {
                score = value;
                ScoreChanged?.Invoke(score);
            }
        }

        public static Vector2 WorldExtents
        {
            get;
            private set;
        }

        public static event Action<int> LivesChanged;
        public static event Action<int> ScoreChanged;

        public static void SpawnExplosion(Vector3 position)
        {
            if (Current && Current.explosionPrefab && IsQuitting == false)
            {
                Instantiate(Current.explosionPrefab, position, Quaternion.identity);
            }
        }

        public void SpawnEnemy()
        {
            if (enemyPrefab)
            {
                Instantiate(enemyPrefab);
            }

            timeTillEnemySpawn = Rand.Range(minEnemySpawnDelay, maxEnemySpawnDelay);
        }

        public void StartNewGame()
        {
            MainCanvas.Hide(() =>
            {
                WorldCanvas.Show(() =>
                {
                    SpawnShip();
                    SpawnAsteroidWave();
                });
            });

            IsPlaying = true;
            Score = 0;
            Lives = numberOfLives;
            timeTillEnemySpawn = Rand.Range(minEnemySpawnDelay, maxEnemySpawnDelay);
        }

        protected override void Awake()
        {
            base.Awake();

            Controls = new Controls();
        }

        private static void OnApplicationQuitting()
        {
            IsQuitting = true;
        }

        private void OnAsteroidDestroyed()
        {
            if (Asteroid.List.Count == 0 && IsPlaying)
            {
                SpawnAsteroidWave();
            }
        }

        private void OnDisable()
        {
            Application.quitting -= OnApplicationQuitting;
            Ship.Destroyed -= OnShipDestroyed;
            Asteroid.Destroyed -= OnAsteroidDestroyed;

            Controls.Disable();
        }

        private void OnEnable()
        {
            Application.quitting += OnApplicationQuitting;
            Ship.Destroyed += OnShipDestroyed;
            Asteroid.Destroyed += OnAsteroidDestroyed;

            Controls.Enable();
        }

        private void OnShipDestroyed()
        {
            if (Lives > 0)
            {
                Invoke("SpawnShip", respawnDelay);
            }
            else
            {
                IsPlaying = false;

                foreach (var asteroid in Asteroid.List)
                {
                    asteroid.SpriteFader.FadeOut(() => Destroy(asteroid.gameObject));
                }

                if (Enemy.Current)
                {
                    Enemy.Current.SpriteFader.FadeOut(() => Destroy(Enemy.Current.gameObject));
                }

                WorldCanvas.Hide();
                MainCanvas.Show();
                StartDisplay.ShowGameOver();
            }
        }

        private void SpawnAsteroid()
        {
            if (asteroidPrefab && IsQuitting == false)
            {
                if (Instantiate(asteroidPrefab).TryGetComponent(out Asteroid asteroid))
                {
                    asteroid.SetPositionAlongScreenEdge();
                }
            }
        }

        private void SpawnAsteroidWave()
        {
            for (var i = 0; i < asteroidWaveCount; i++)
            {
                SpawnAsteroid();
            }
        }

        private void SpawnShip()
        {
            if (shipPrefab && IsQuitting == false)
            {
                Instantiate(shipPrefab);
            }
        }

        private void Start()
        {
            WorldExtents = Camera.main.ViewportToWorldPoint(Vector2.one);

            MainCanvas.Show();
            StartDisplay.ShowStart();
        }

        private void Update()
        {
            if (IsPlaying)
            {
                timeTillEnemySpawn -= Time.deltaTime;

                if (timeTillEnemySpawn <= 0f && Enemy.Current == null)
                {
                    SpawnEnemy();
                }
            }
            else
            {
                if (Controls.Primary.Start.triggered)
                {
                    StartNewGame();
                }
            }
        }
    }
}