﻿using Abstracts;
using UnityEngine;

namespace UI.Menu
{
    public class StartDisplay : SingletonBehaviour<StartDisplay>
    {
        [SerializeField] private GameObject gameOver = default;

        public static void ShowGameOver()
        {
            if (Current && Current.gameOver)
            {
                Current.gameOver.SetActive(true);
            }
        }

        public static void ShowStart()
        {
            if (Current && Current.gameOver)
            {
                Current.gameOver.SetActive(false);
            }
        }
    }
}