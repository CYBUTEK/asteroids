﻿using Managers;
using UnityEngine;
using Utilities;

namespace Actors
{
    public class Explosion : MonoBehaviour
    {
        [Header("SFX")]
        [SerializeField] private AudioClip audioClip = default;
        [SerializeField] private float audioPitchVariation = default;

        public ParticleSystem ParticleSystem
        {
            get;
            private set;
        }

        private void Awake()
        {
            ParticleSystem = GetComponent<ParticleSystem>();
        }

        private void Start()
        {
            AudioManager.PlaySfx(audioClip, 1f, Rand.Pitch(audioPitchVariation));
        }

        private void Update()
        {
            if (ParticleSystem && ParticleSystem.isStopped)
            {
                Destroy(gameObject);
            }
        }
    }
}