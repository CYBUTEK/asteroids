﻿using Abstracts;
using UnityEngine;

namespace Components
{
    public class SpriteFader : AbstractFader
    {
        public override float Alpha
        {
            get => SpriteRenderer ? SpriteRenderer.color.a : 1f;
            set
            {
                if (SpriteRenderer)
                {
                    var color = SpriteRenderer.color;
                    color.a = Mathf.Clamp(value, MinimumAlpha, MaximumAlpha);
                    SpriteRenderer.color = color;
                }
            }
        }

        public override bool IsFadeEnabled
        {
            get => SpriteRenderer;
        }

        public SpriteRenderer SpriteRenderer
        {
            get;
            private set;
        }

        private void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }
    }
}