﻿using UnityEngine;
using Utilities;

namespace Components
{
    public class CameraShake : MonoBehaviour
    {
        private static float magnitude;

        [SerializeField] private float rate = default;
        [SerializeField] private float damping = default;

        private float elapsedTime;
        private Vector3 originPosition;

        public static float Magnitude
        {
            get => magnitude;
            set => magnitude = Mathf.Clamp(value, 0f, float.MaxValue);
        }

        private void Start()
        {
            originPosition = transform.position;
            magnitude = 0f;
        }

        private void Update()
        {
            elapsedTime += Time.deltaTime * rate;

            if (elapsedTime >= 1f)
            {
                elapsedTime = 0f;
                transform.position = originPosition + (Vector3)Rand.Direction * Magnitude;
            }

            Magnitude -= damping * Time.deltaTime;
        }
    }
}