﻿using Abstracts;
using Managers;
using UnityEngine.UI;

namespace UI.Game
{
    public class ScoreDisplay : SingletonBehaviour<ScoreDisplay>
    {
        public Text TextComponent
        {
            get;
            private set;
        }

        protected override void Awake()
        {
            base.Awake();

            TextComponent = GetComponent<Text>();
        }

        private void OnDisable()
        {
            GameManager.ScoreChanged -= OnScoreChanged;
        }

        private void OnEnable()
        {
            GameManager.ScoreChanged += OnScoreChanged;
            OnScoreChanged(GameManager.Score);
        }

        private void OnScoreChanged(int score)
        {
            if (TextComponent)
            {
                TextComponent.text = $"{score:#000000}";
            }
        }
    }
}