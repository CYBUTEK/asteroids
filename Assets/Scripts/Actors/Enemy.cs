﻿using System.Collections;
using Abstracts;
using Components;
using Managers;
using UnityEngine;
using Utilities;

namespace Actors
{
    public class Enemy : SingletonBehaviour<Enemy>
    {
        [SerializeField] private int score = default;
        [SerializeField] private float destroyShakeMagnitude = default;
        [SerializeField] private float speed = default;
        [SerializeField] private int shotsPerBurst = default;
        [SerializeField] private float timeBetweenBursts = default;
        [SerializeField] private float timeBetweenBurstShots = default;
        [SerializeField] private GameObject bulletPrefab = default;

        private bool becameInvisible;

        public Rigidbody2D Rigidbody2D { get; private set; }

        public SpriteFader SpriteFader { get; private set; }

        public SpriteRenderer SpriteRenderer { get; private set; }

        protected override void Awake()
        {
            base.Awake();

            Rigidbody2D = GetComponent<Rigidbody2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            SpriteFader = GetComponent<SpriteFader>();
        }

        private void OnBecameInvisible()
        {
            becameInvisible = true;
            Destroy(gameObject);
        }

        private void OnDestroy()
        {
            if (GameManager.IsPlaying && becameInvisible == false)
            {
                GameManager.SpawnExplosion(transform.position);
                CameraShake.Magnitude += destroyShakeMagnitude;
                GameManager.Score += score;
            }
        }

        private IEnumerator ShootCoroutine()
        {
            if (bulletPrefab)
            {
                while (true)
                {
                    for (var i = 0; i < shotsPerBurst; i++)
                    {
                        if (Ship.Current)
                        {
                            var angle = Vector2.SignedAngle(Vector2.up, Ship.Current.transform.position - transform.position);
                            var rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                            if (Instantiate(bulletPrefab, transform.position, rotation).TryGetComponent(out Bullet bullet))
                            {
                                bullet.InteractsWithPlayer = true;
                            }

                            yield return new WaitForSeconds(timeBetweenBurstShots);
                        }
                    }

                    yield return new WaitForSeconds(timeBetweenBursts);
                }
            }
        }

        private void Start()
        {
            var worldExtents = GameManager.WorldExtents;
            var spriteExtents = SpriteRenderer.bounds.extents;
            var position = transform.position;

            position.x = Rand.Value(-worldExtents.x - spriteExtents.x, worldExtents.x + spriteExtents.x);
            position.y = Rand.Range(-worldExtents.y, worldExtents.y);

            transform.position = position;

            if (Rigidbody2D)
            {
                Rigidbody2D.velocity = (position.x > 0f ? Vector2.left : Vector2.right) * speed;
            }

            if (SpriteFader)
            {
                SpriteFader.FadeIn();
            }

            StartCoroutine(ShootCoroutine());
        }
    }
}