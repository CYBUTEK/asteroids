﻿using Abstracts;
using Managers;
using UnityEngine;

namespace UI.Game
{
    public class LivesDisplay : SingletonBehaviour<LivesDisplay>
    {
        [SerializeField] private GameObject livesDisplayIconPrefab = default;

        private void OnDisable()
        {
            GameManager.LivesChanged -= OnLivesChanged;
        }

        private void OnEnable()
        {
            GameManager.LivesChanged += OnLivesChanged;
            OnLivesChanged(GameManager.Lives);
        }

        private void OnLivesChanged(int lives)
        {
            if (livesDisplayIconPrefab && lives >= 0)
            {
                while (lives > transform.childCount)
                {
                    Instantiate(livesDisplayIconPrefab, transform);
                }

                while (lives < transform.childCount)
                {
                    DestroyImmediate(transform.GetChild(0).gameObject);
                }
            }
        }
    }
}