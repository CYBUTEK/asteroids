﻿using System;
using System.Collections;
using UnityEngine;

namespace Abstracts
{
    public abstract class AbstractFader : MonoBehaviour
    {
        [SerializeField] private float speed = default;
        [SerializeField] private float startingAlpha = default;
        [SerializeField, Range(0f, 1f)] private float minimumAlpha;
        [SerializeField, Range(0f, 1f)] private float maximumAlpha = 1f;

        private IEnumerator fadeCoroutine;

        public abstract float Alpha
        {
            get;
            set;
        }

        public abstract bool IsFadeEnabled
        {
            get;
        }

        public bool IsFading
        {
            get => fadeCoroutine != null;
        }

        public float MaximumAlpha
        {
            get => maximumAlpha;
            set => maximumAlpha = value;
        }

        public float MinimumAlpha
        {
            get => minimumAlpha;
            set => minimumAlpha = value;
        }

        public void FadeIn(Action callback = null)
        {
            StartFade(0f, 1f, callback);
        }

        public void FadeOut(Action callback = null)
        {
            StartFade(1f, 0f, callback);
        }

        public void StartFade(float from, float to, Action callback)
        {
            if (fadeCoroutine != null)
            {
                StopCoroutine(fadeCoroutine);
            }

            fadeCoroutine = FadeCoroutine(from, to, callback);
            StartCoroutine(fadeCoroutine);
        }

        protected virtual void Start()
        {
            Alpha = startingAlpha;
        }

        private IEnumerator FadeCoroutine(float from, float to, Action callback)
        {
            if (IsFadeEnabled)
            {
                from = Mathf.Clamp(from, minimumAlpha, maximumAlpha);
                to = Mathf.Clamp(to, minimumAlpha, maximumAlpha);

                var progress = 0f;

                while (progress < 1f)
                {
                    Alpha = Mathf.Lerp(from, to, progress);
                    yield return null;
                    progress += Time.deltaTime * speed;
                }

                Alpha = to;
            }

            fadeCoroutine = null;
            callback?.Invoke();
        }
    }
}