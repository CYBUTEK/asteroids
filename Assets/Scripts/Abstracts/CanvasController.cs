﻿using System;
using Components;
using UnityEngine;

namespace Abstracts
{
    public abstract class CanvasController<T> : SingletonBehaviour<T> where T : CanvasController<T>
    {
        [SerializeField] private bool startDisabled = default;
        [SerializeField] private bool dontDestroyOnLoad = default;

        public CanvasGroupFader CanvasGroupFader
        {
            get;
            private set;
        }

        public static void Hide(Action callback = null)
        {
            if (Current)
            {
                if (Current.CanvasGroupFader)
                {
                    Current.CanvasGroupFader.FadeOut(() =>
                    {
                        Current.gameObject.SetActive(false);
                        callback?.Invoke();
                    });
                }
                else
                {
                    Current.gameObject.SetActive(false);
                    callback?.Invoke();
                }
            }
            else
            {
                callback?.Invoke();
            }
        }

        public static void Show(Action callback = null)
        {
            if (Current)
            {
                Current.gameObject.SetActive(true);

                if (Current.CanvasGroupFader)
                {
                    Current.CanvasGroupFader.FadeIn(callback);
                }
                else
                {
                    callback?.Invoke();
                }
            }
            else
            {
                callback?.Invoke();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            CanvasGroupFader = GetComponent<CanvasGroupFader>();
        }

        protected virtual void Start()
        {
            if (startDisabled)
            {
                gameObject.SetActive(false);
            }

            if (dontDestroyOnLoad)
            {
                DontDestroyOnLoad(this);
            }
        }
    }
}