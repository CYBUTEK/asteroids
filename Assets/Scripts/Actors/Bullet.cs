﻿using Managers;
using UnityEngine;
using Utilities;

namespace Actors
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float speed = default;
        [SerializeField] private float timeToLive = default;

        [Header("SFX")]
        [SerializeField] private AudioClip audioClip = default;
        [SerializeField, Range(0f, 1f)] private float audioVolume = default;
        [SerializeField] private float audioPitchVariation = default;

        public bool InteractsWithEnemy { get; set; }

        public bool InteractsWithPlayer { get; set; }

        public Rigidbody2D Rigidbody2D { get; private set; }

        private void Awake()
        {
            Rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out Asteroid asteroid))
            {
                Destroy(asteroid.gameObject);
                Destroy(gameObject);
            }

            if (InteractsWithEnemy && collision.TryGetComponent(out Enemy enemy))
            {
                Destroy(enemy.gameObject);
                Destroy(gameObject);
            }

            if (InteractsWithPlayer && collision.TryGetComponent(out Ship ship))
            {
                Destroy(ship.gameObject);
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            if (Rigidbody2D)
            {
                Rigidbody2D.velocity += (Vector2)transform.up * speed;
            }

            AudioManager.PlaySfx(audioClip, audioVolume, Rand.Pitch(audioPitchVariation));

            Destroy(gameObject, timeToLive);
        }
    }
}