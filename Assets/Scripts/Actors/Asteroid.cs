﻿using System;
using System.Collections.Generic;
using Components;
using Managers;
using UnityEngine;
using Utilities;

namespace Actors
{
    public class Asteroid : MonoBehaviour
    {
        [SerializeField] private int score = default;
        [SerializeField] private Sprite[] sprites = default;

        [Header("Physics")]
        [SerializeField] private float maxSpeed = default;
        [SerializeField] private float maxAngularVelocity = default;

        [Header("Debris")]
        [SerializeField] private int debrisCount = default;
        [SerializeField] private GameObject debrisPrefab = default;

        [Header("Camera Shake")]
        [SerializeField] private float destroyShakeMagnitude = default;

        public static List<Asteroid> List { get; } = new List<Asteroid>();

        public PolygonCollider2D PolygonCollider2D
        {
            get;
            private set;
        }

        public Rigidbody2D Rigidbody2D
        {
            get;
            private set;
        }

        public SpriteFader SpriteFader
        {
            get;
            private set;
        }

        public SpriteRenderer SpriteRenderer
        {
            get;
            private set;
        }

        public static event Action Destroyed;

        public void SetPositionAlongScreenEdge()
        {
            var worldExtents = GameManager.WorldExtents;
            var position = transform.position;

            if (Rand.Bool)
            {
                position.x = Rand.Value(-worldExtents.x, worldExtents.x);
                position.y = Rand.Range(-worldExtents.y, worldExtents.y);
            }
            else
            {
                position.x = Rand.Range(-worldExtents.x, worldExtents.y);
                position.y = Rand.Value(-worldExtents.y, worldExtents.y);
            }

            transform.position = position;
        }

        private void Awake()
        {
            Rigidbody2D = GetComponent<Rigidbody2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            SpriteFader = GetComponent<SpriteFader>();

            List.Add(this);
        }

        private void OnDestroy()
        {
            if (GameManager.IsPlaying)
            {
                GameManager.SpawnExplosion(transform.position);
                CameraShake.Magnitude += destroyShakeMagnitude;
                GameManager.Score += score;

                if (debrisPrefab && GameManager.IsQuitting == false)
                {
                    for (var i = 0; i < debrisCount; i++)
                    {
                        Instantiate(debrisPrefab, transform.position, Quaternion.identity);
                    }
                }
            }

            List.Remove(this);
            Destroyed?.Invoke();
        }

        private void Start()
        {
            if (SpriteRenderer && sprites.Length > 0)
            {
                SpriteRenderer.sprite = Rand.Value(sprites);
            }

            PolygonCollider2D = gameObject.AddComponent<PolygonCollider2D>();

            transform.rotation = Rand.Rotation;

            if (Rigidbody2D)
            {
                Rigidbody2D.velocity = Rand.Direction * maxSpeed;
                Rigidbody2D.angularVelocity = Rand.Range(-maxAngularVelocity, maxAngularVelocity);
            }

            if (SpriteFader)
            {
                SpriteFader.FadeIn();
            }
        }
    }
}