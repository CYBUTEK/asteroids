﻿using Managers;
using UnityEngine;

namespace Components
{
    public class ScreenWrapper : MonoBehaviour
    {
        public SpriteRenderer SpriteRenderer
        {
            get;
            private set;
        }

        private void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            var worldExtents = GameManager.WorldExtents;
            var spriteExtents = SpriteRenderer.bounds.extents;
            var position = transform.position;

            // horizontal
            if (position.x + spriteExtents.x < -worldExtents.x)
            {
                position.x = worldExtents.x + spriteExtents.x;
            }
            else if (position.x - spriteExtents.x > worldExtents.x)
            {
                position.x = -worldExtents.x - spriteExtents.x;
            }

            // vertical
            if (position.y + spriteExtents.y < -worldExtents.y)
            {
                position.y = worldExtents.y + spriteExtents.y;
            }
            else if (position.y - spriteExtents.y > worldExtents.y)
            {
                position.y = -worldExtents.y - spriteExtents.y;
            }

            transform.position = position;
        }
    }
}