﻿using UnityEngine;

namespace Abstracts
{
    public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Current
        {
            get;
            private set;
        }

        protected virtual void Awake()
        {
            if (Current == null)
            {
                Current = this as T;
            }
            else if (Current != this)
            {
                Destroy(gameObject);
            }
        }
    }
}