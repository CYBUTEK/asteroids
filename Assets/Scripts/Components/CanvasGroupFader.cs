﻿using Abstracts;
using UnityEngine;

namespace Components
{
    public class CanvasGroupFader : AbstractFader
    {
        public override float Alpha
        {
            get => CanvasGroup ? CanvasGroup.alpha : 1f;
            set
            {
                if (CanvasGroup)
                {
                    CanvasGroup.alpha = Mathf.Clamp(value, MinimumAlpha, MaximumAlpha);
                }
            }
        }

        public CanvasGroup CanvasGroup
        {
            get;
            private set;
        }

        public override bool IsFadeEnabled
        {
            get => CanvasGroup;
        }

        private void Awake()
        {
            CanvasGroup = GetComponent<CanvasGroup>();
        }
    }
}