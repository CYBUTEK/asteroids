﻿using System.Collections.Generic;
using Abstracts;
using UnityEngine;

namespace Managers
{
    public class AudioManager : SingletonBehaviour<AudioManager>
    {
        [SerializeField] private AudioClip[] musicTracks = default;

        private AudioSource musicAudioSource;
        private int musicTrackIndex;
        private List<AudioSource> sfxAudioSources = new List<AudioSource>();

        public static void PlaySfx(AudioClip clip, float volume, float pitch)
        {
            if (Current)
            {
                var audioSource = Current.GetSfxAudioSource();
                audioSource.clip = clip;
                audioSource.volume = volume;
                audioSource.pitch = pitch;
                audioSource.Play();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            musicAudioSource = gameObject.AddComponent<AudioSource>();
        }

        private AudioSource GetSfxAudioSource()
        {
            foreach (var sfxAudioSource in sfxAudioSources)
            {
                if (sfxAudioSource.isPlaying == false)
                {
                    return sfxAudioSource;
                }
            }

            var audioSource = gameObject.AddComponent<AudioSource>();
            sfxAudioSources.Add(audioSource);
            return audioSource;
        }

        private void Update()
        {
            if (musicAudioSource && musicAudioSource.isPlaying == false && musicTracks.Length > 0)
            {
                musicTrackIndex = (musicTrackIndex + 1) % musicTracks.Length;
                musicAudioSource.clip = musicTracks[musicTrackIndex];
                musicAudioSource.Play();
            }
        }
    }
}