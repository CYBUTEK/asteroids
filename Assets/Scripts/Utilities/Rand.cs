﻿using UnityEngine;

namespace Utilities
{
    public static class Rand
    {
        public static bool Bool
        {
            get => Random.value > 0.5f;
        }

        public static Vector2 Direction
        {
            get => Random.insideUnitCircle.normalized;
        }

        public static Quaternion Rotation
        {
            get => Quaternion.AngleAxis(Random.value * 360f, Vector3.forward);
        }

        public static float Pitch(float variation)
        {
            return Random.Range(1f - variation, 1f + variation);
        }

        public static float Range(float min, float max)
        {
            return Random.Range(min, max);
        }

        public static T Value<T>(params T[] values)
        {
            return values[Random.Range(0, values.Length)];
        }
    }
}